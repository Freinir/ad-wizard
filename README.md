# README #

This README would normally document whatever steps are necessary to get your application up and running.

### About ###

This library provides an interface for rendering different ads for mobile versions and desktops.

### Installation ###

Install the package via Composer

composer require freinir/ad-wizard


### Basic usage ###

~~~
$yandexAd = new AdYandex('R-XXX-XX')
~~~

Insert into your views

~~~
<?=$yandexAd->getDesktop(1, 'My first RTB block', 'adv__custom')?>
~~~