<?php

namespace Freinir\AdWizard\params;

class GoogleAdParams
{
    public $width, $height, $language, $maxNumAds;
    
    public function __construct(int $width, int $height, int $maxNumAds = 1, $language = 'ru')
    {
        $this->width = $width;
        $this->height = $height;
        $this->maxNumAds = $maxNumAds;
        $this->language = $language;
    }
}