<?php

namespace Freinir\AdWizard\params;

class AdaptiveGoogleParams
{
    public $language, $maxNumAds;
    
    public function __construct(int $maxNumAds = 1, $language = 'ru')
    {
        $this->maxNumAds = $maxNumAds;
        $this->language = $language;
    }
}