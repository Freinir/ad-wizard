<?php
namespace Freinir\AdWizard;

abstract class AbstractAd implements AdInterface {
    /**
     * @var \Mobile_Detect
     */
    protected $isMobile;
    
    protected $partnerId;
    
    public const TYPE_DESKTOP = 1;
    public const TYPE_MOBILE = 2;
    public const TYPE_ALL = 3;
    
    public function __construct($partnerId)
    {
        $this->isMobile = (new \Mobile_Detect())->isMobile();
        $this->partnerId = $partnerId;
    }
    
    protected function render(array $args, $templatePath)
    {
        if (isset($args['comment'])) {
            $args['comment'] = $this->cleanComment($args['comment']);
        } else {
            $args['comment'] = '';
        }
        
        
        extract($args,EXTR_SKIP);
        ob_start();
        include($templatePath);
        return ob_get_clean();
    }
    
    private function cleanComment($comment)
    {
        return str_replace(['<!--', '-->'], '', $comment);
    }
}