<?php

namespace Freinir\AdWizard;

use Freinir\AdWizard\params\GoogleAdParams;

class AdGoogle extends AbstractAd
{
    private $hasBadContent;
    
    public function __construct($partnerId, $hasBadContent)
    {
        parent::__construct($partnerId);
        
        $this->hasBadContent = $hasBadContent;
    }
    
    /**
     * @param int $adId
     * @param string|null $comment
     * @param string|null $className
     * @param GoogleAdParams $params
     * @return string
     */
    public function getDesktop(int $adId, string $comment = null, string $className = null, $params = null): string
    {
        if (!$this->isMobile) {
            return $this->render([
                'partnerId' => $this->partnerId,
                'blockId' => $adId,
                'comment' => $comment,
                'className' => $className,
                'params' => $params
            ], __DIR__ . '/templates/google-ad.php');
        }
        return '';
    }
    
    public function getMobile(int $adId, string $comment = null, string $className = null, $params = null): string
    {
        if ($this->isMobile) {
            return $this->render([
                'partnerId' => $this->partnerId,
                'blockId' => $adId,
                'comment' => $comment,
                'className' => $className,
                'params' => $params
            ], __DIR__ . '/templates/google-ad.php');
        }
        return '';
    }
    
    public function getAllSize(int $adId, string $comment, string $className = null, $params = null): string
    {
        
        return $this->render([
            'partnerId' => $this->partnerId,
            'blockId' => $adId,
            'comment' => $comment,
            'className' => $className,
            'params' => $params
        ], __DIR__ . '/templates/google-ad.php');
    }
    
    public function getAdaptive(int $adId, string $comment, string $className = null, $params = null): string
    {
        return $this->render([
            'partnerId' => $this->partnerId,
            'blockId' => $adId,
            'comment' => $comment,
            'className' => $className,
            'params' => $params
        ], __DIR__ . '/templates/google-ad-adaptive.php');
    }
    
    public function getCustomAdaptive(int $adId, string $comment, $attributes, $screenType = null, $className = null): string
    {
        if ($screenType === self::TYPE_DESKTOP & $this->isMobile) {
            return '';
        }
        
        if ($screenType === self::TYPE_MOBILE && !$this->isMobile) {
            return '';
        }
        
        return $this->render([
            'partnerId' => $this->partnerId,
            'blockId' => $adId,
            'comment' => $comment,
            'attributes' => $attributes,
            'className' => $className
        ], __DIR__ . '/templates/google-custom-adapt.php');
    }
    
    /**
     * @param array $args
     * @return string
     */
    public function render(array $args, $templatePath): string
    {
        if (!isset($args['params']) && !isset($args['attributes'])) {
            return 'Missing ad parameters';
        }
        
        if (!$this->hasBadContent) {
            return parent::render($args, $templatePath);
        }
        
        return '';
    }
}