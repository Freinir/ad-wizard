<?php
namespace Freinir\AdWizard;

interface AdInterface {
    
    public function getDesktop(int $adId,
                               string $comment = null,
                               string $className = null,
                               $params = null): string;
    
    public function getMobile(int $adId,
                              string $comment = null,
                              string $className = null,
                              $params = null): string ;
    
    public function getAllSize(int $adId,
                               string $comment,
                               string $className = null,
                               $params = null): string;
    
}