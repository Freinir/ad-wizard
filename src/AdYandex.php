<?php

namespace Freinir\AdWizard;
class AdYandex extends AbstractAd
{
    public function __construct($partnerId)
    {
        parent::__construct($partnerId);
    }
    
    /**
     * Return advertisement in desktop and tablets
     * @param int $adId
     * @param string|null $comment
     * @param string|null $className
     * @param null $params
     * @return string
     */
    public function getDesktop(int $adId, string $comment = null, string $className = null, $params = null): string
    {
        if (!$this->isMobile) {
            return $this->render([
                'partnerId' => $this->partnerId,
                'blockId' => $adId,
                'comment' => $comment,
                'className' => $className
                ], __DIR__ . '/templates/rtb.php');
        }
        return '';
    }
    
    /**
     * Return advertisement in mobile device
     * @param int $adId
     * @param string|null $comment
     * @param string|null $className
     * @param null $params
     * @return string
     */
    public function getMobile(int $adId, string $comment = null, string $className = null, $params = null): string
    {
        if ($this->isMobile) {
            return $this->render([
                'partnerId' => $this->partnerId,
                'blockId' => $adId,
                'comment' => $comment,
                'className' => $className
            ], __DIR__ . '/templates/rtb.php');
        }
        return '';
    }
    
    /**
     * Return advertisement for desktop or mobile
     * @param int $desktopAdId
     * @param int $mobileAdId
     * @param string|null $comment
     * @param string|null $className
     * @param null $params
     * @return string
     */
    public function getDesktopOrMobile(int $desktopAdId, int $mobileAdId, string $comment = null, string $className = null, $params = null): string
    {
        if ($this->isMobile) {
            return $this->render([
                'partnerId' => $this->partnerId,
                'blockId' => $mobileAdId,
                'comment' => $comment,
                'className' => $className
            ], __DIR__ . '/templates/rtb.php');
        }
        return $this->render([
            'partnerId' => $this->partnerId,
            'blockId' => $desktopAdId,
            'comment' => $comment,
            'className' => $className
        ]);
    }
    
    /**
     * @param int $adId
     * @param string $comment
     * @param string|null $className
     * @param null $params
     * @return string
     */
    public function getAllSize(int $adId, string $comment, string $className = null, $params = null): string
    {
        return $this->render([
            'partnerId' => $this->partnerId,
            'blockId' => $adId,
            'comment' => $comment,
            'className' => $className
        ], __DIR__ . '/templates/rtb.php');
    }
    
    /**
     * @param int $adId
     * @param string $comment
     * @param string|null $className
     * @param null $params
     * @return string
     */
    public function getMobileSticky(int $adId, string $comment): string
    {
        if ($this->isMobile) {
            return $this->render([
                'partnerId' => $this->partnerId,
                'blockId' => $adId,
                'comment' => $comment,
            ], __DIR__ . '/templates/sticky.php');
        }
        return '';
    }
    
    public function getDesktopSticky(int $adId, string $comment): string
    {
        if (!$this->isMobile) {
            return $this->render([
                'partnerId' => $this->partnerId,
                'blockId' => $adId,
                'comment' => $comment,
            ], __DIR__ . '/templates/sticky.php');
        }
        return '';
    }
    
    public function getMobileFullscreen(int $adId): string
    {
        if ($this->isMobile) {
            return $this->render([
                'partnerId' => $this->partnerId,
                'blockId' => $adId
            ], __DIR__ . '/templates/fullscreen.php');
        }
        return '';
    }
    
    public function getDesktopFullscreen(int $adId): string
    {
        if (!$this->isMobile) {
            return $this->render([
                'partnerId' => $this->partnerId,
                'blockId' => $adId
            ], __DIR__ . '/templates/fullscreen.php');
        }
        return '';
    }
}