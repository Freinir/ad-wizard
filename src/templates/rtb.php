<?php
/**
 * @var $comment
 * @var $partnerId
 * @var $blockId
 * @var $className
 */
?>

<?php if(!empty($comment)):?>
<!-- <?=$comment?> -->
<?php endif;?>
<!-- Yandex.RTB <?=$partnerId?>-<?=$blockId?> -->
<div id="yandex_rtb_<?=$partnerId?>-<?=$blockId?>" class="rtb__block <?=$className?>"></div>
<script>
  window.yaContextCb.push(() => {
    Ya.Context.AdvManager.render({
      "blockId": "<?=$partnerId?>-<?=$blockId?>",
      "renderTo": "yandex_rtb_<?=$partnerId?>-<?=$blockId?>",
    })
  })
</script>