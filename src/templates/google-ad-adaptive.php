<?php
/**
 * @var $comment
 * @var $partnerId
 * @var $blockId
 * @var $className
 * @var \Freinir\AdWizard\params\AdaptiveGoogleParams $params
 */
?>

<?php if(!empty($className)):?>
<div class="<?=$className?>">
<?php endif;?>

<?php if(!empty($comment)):?>
    <!--<?=$comment?>-->
<?php endif;?>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="<?=$partnerId?>"
     data-ad-slot="<?=$blockId?>"
     google-language="<?=$params->language?>"
     google-max-num-ads="<?=$params->maxNumAds?>"
     data-ad-format="auto"></ins>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({});
</script>

<?php if(!empty($className)):?>
</div>
<?php endif;?>
