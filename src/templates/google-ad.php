<?php
/**
 * @var $comment
 * @var $partnerId
 * @var $blockId
 * @var $className
 * @var \Freinir\AdWizard\params\GoogleAdParams $params
 */
?>

<?php if(!empty($className)):?>
<div class="<?=$className?>">
<?php endif;?>

    <?php if(!empty($comment)):?>
        <!--<?=$comment?>-->
    <?php endif;?>

    <script type="text/javascript">
        google_ad_client = "<?=$partnerId?>";
        google_ad_slot = "<?=$blockId?>";
        google_ad_width = <?=$params->width?>;
        google_ad_height = <?=$params->height?>;
        google_language ="<?=$params->language?>";
        google_max_num_ads = <?=$params->maxNumAds?>;
    </script>
    <script type="text/javascript" src="//pagead2.googlesyndication.com/pagead/show_ads.js"></script>

<?php if(!empty($className)):?>
</div>
<?php endif;?>