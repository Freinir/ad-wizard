<?php
/**
 * @var $comment
 * @var $partnerId
 * @var $blockId
 * @var $attributes
 */
?>

<?php if(!empty($comment)):?>
    <!--<?=$comment?>-->
<?php endif;?>

<?php if(!empty($className)):?>
<div class="<?=$className?>">
<?php endif;?>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
    style="display:block"
    data-ad-client="<?=$partnerId?>"
    data-ad-slot="<?=$blockId?>"
    <?php foreach ($attributes as $key => $value):?>
        <?=$key?>="<?=$value?>"
    <?php endforeach;?>
    ></ins>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({});
</script>

<?php if(!empty($className)):?>
</div>
<?php endif;?>
